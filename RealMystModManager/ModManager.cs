using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace RealMystModManager
{
    /// <summary>
    /// Loaded and added as a component by the hacked Assembly-CSharp.dll, this script
    /// in turn loads all the other mods.
    /// 
    /// Ideally we would rely on AppDomains to provide sandboxing of mods, which would
    /// be more secure on top of making it possible to activate and deactivate mods at runtime.
    /// Buuuut, for some reason, the version of Mono RMME uses crashes when creating new AppDomains.
    /// This sucks big time.
    /// </summary>
    public class ModManager : MonoBehaviour
    {
        /// <summary>
        /// Provides some basic infos about a loaded mod.
        /// </summary>
        public class ModInfos
        {
            public string DllPath { get; private set; }
            public string DllName { get; private set; }
            public AppDomain Domain { get; private set; }
            public Assembly Assembly { get; private set; }
            public GameObject ModObject { get; private set; }
            public MonoBehaviour ModBehaviour { get; private set; }
            public IMod ModInterface { get; private set; }

            public ModInfos(string dllPath)
            {
                if (string.IsNullOrEmpty(dllPath))
                    throw new ArgumentNullException("dllPath must be a valid URL.");
                DllPath = dllPath;
                DllName = Path.GetFileNameWithoutExtension(dllPath);
            }

            public void Load()
            {
                Domain = AppDomain.CurrentDomain;
                Assembly = Assembly.LoadFrom(DllPath);
                ModObject = new GameObject(DllName);

                List<Type> validTypes = Assembly
                    .GetTypes()
                    .Where(type =>
                        type.IsClass &&
                        !type.IsAbstract &&
                        type.IsSubclassOf(typeof(MonoBehaviour)) &&
                        type.GetInterfaces().Contains(typeof(IMod))).ToList();

                if (validTypes.Count != 1)
                    throw new ArgumentOutOfRangeException("A mod DLL must have one and only one class implementing both IMod and MonoBehaviour.");

                ModObject = new GameObject(Domain.FriendlyName);
                DontDestroyOnLoad(ModObject);
                ModBehaviour = (MonoBehaviour)ModObject.AddComponent(validTypes[0]);
                ModInterface = (IMod)ModBehaviour;
            }
        }

        /// <summary>
        /// A reference to the only instance of this manager.
        /// </summary>
        public static ModManager Instance { get; private set; }

        readonly List<ModInfos> mods = new List<ModInfos>();
        readonly List<Assembly> dependencyAssemblies = new List<Assembly>();
        string modsPath;
        string dependencyPath;
        readonly Queue<KeyValuePair<float, string>> logMessages = new Queue<KeyValuePair<float, string>>();
        readonly float messagesDisplayTime = 5;
        bool showHelpDialog = false;
        Vector2 helpViewScroll;
        ModInfos helpDialogFocusedMod;

#pragma warning disable IDE0051
        void Awake()
#pragma warning restore IDE0051
        {
            if (Instance != null)
            {
                Destroy(this);
                throw new Exception("Only one instance of ModManager can be instanced.");
            }
            Instance = this;

            Log("Mod manager loaded.");
            DontDestroyOnLoad(gameObject);
        }

#pragma warning disable IDE0051
        void Start()
#pragma warning restore IDE0051
        {
            Log("Mod manager: loading mods...");

            // Find the mods path. This is usually located in the persistent data paths (AppData/LocalLow/Cyan Worlds/realMyst/mods).
            modsPath = Path.Combine(Application.persistentDataPath, "mods");
            dependencyPath = Path.Combine(modsPath, "dependencies");

            // just in case...
            Directory.CreateDirectory(modsPath);
            Directory.CreateDirectory(dependencyPath);

            // Register ourself as an assembly resolver (this way we can load DLLs from a custom location).
            AppDomain.CurrentDomain.AssemblyResolve += Domain_AssemblyResolve;

            // Search all mod DLLs.
            foreach (string modDll in Directory.GetFiles(modsPath, "*.dll", SearchOption.TopDirectoryOnly))
            {
                string modName = Path.GetFileNameWithoutExtension(modDll);
                mods.Add(new ModInfos(modDll));
            }

            Log(string.Format("Mod manager: {0} mods found. Proceeding to load...", mods.Count));

            // And now load those DLLs.
            foreach (ModInfos mod in mods)
            {
                Log("Loading: " + mod.DllName);
                mod.Load();
            }

            Log(string.Format("Mod manager: {0} mods loaded.\nPress Ctrl-H for more informations about mods.", mods.Count));
        }

        Assembly Domain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            // ignore missing resources
            if (args.Name.Contains(".resources"))
                return null;

            print(string.Format("Trying to resolve dependency {0}...", args.Name));
            string dependencyDllPath = Path.Combine(dependencyPath, args.Name);
            if (File.Exists(dependencyDllPath))
            {
                Assembly dependencyAssembly = Assembly.LoadFrom(dependencyDllPath);
                dependencyAssemblies.Add(dependencyAssembly);
                return dependencyAssembly;
            }

            Debug.LogWarning(string.Format("Could not resolve dependency {0} !", args.Name));
            return null;
        }

#pragma warning disable IDE0051
        void OnGUI()
#pragma warning restore IDE0051
        {
            DisplayHelpDialog();
            DisplayMessages();
        }

#pragma warning disable IDE0051
        void Update()
#pragma warning restore IDE0051
        {
            if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && Input.GetKeyUp(KeyCode.H))
            {
                showHelpDialog = !showHelpDialog;
                helpDialogFocusedMod = null;
            }
        }

        /// <summary>
        /// Draws the mod list/info dialog.
        /// </summary>
        void DisplayHelpDialog()
        {
            if (!showHelpDialog)
                return;

            Rect rect = new Rect(Screen.width / 4, Screen.height / 4, Screen.width / 2, Screen.height / 2);
            GUILayout.BeginArea(rect, GUI.skin.box);
            helpViewScroll = GUILayout.BeginScrollView(helpViewScroll);

            if (helpDialogFocusedMod != null)
            {
                if (GUILayout.Button("< Return", GUILayout.ExpandWidth(false)))
                {
                    helpDialogFocusedMod = null;
                    return;
                }
                
                GUILayout.Space(18);

                GUILayout.Label(helpDialogFocusedMod.ModInterface.UserFriendlyName);
                GUILayout.Label("version " + helpDialogFocusedMod.ModInterface.Version);
                GUILayout.Label("by " + helpDialogFocusedMod.ModInterface.Author);
                GUILayout.Label("assembly: " + helpDialogFocusedMod.DllName + ".dll");

                GUILayout.Space(18);

                string modUrl = helpDialogFocusedMod.ModInterface.Url;
                if (!string.IsNullOrEmpty(modUrl))
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Website: ");
                    if (GUILayout.Button(modUrl, GUILayout.ExpandWidth(false)))
                        Application.OpenURL(modUrl);
                    GUILayout.FlexibleSpace();
                    GUILayout.EndHorizontal();
                }

                GUILayout.Space(18);
                GUILayout.Label("Description:");
                GUILayout.Label(helpDialogFocusedMod.ModInterface.Description);
                GUILayout.Space(18);
                helpDialogFocusedMod.ModInterface.DrawCustomHelpSheet();
            }
            else
            {
                GUILayout.Label("Active mods:");
                foreach (ModInfos mod in mods)
                {
                    if (GUILayout.Button(mod.ModInterface.UserFriendlyName, GUILayout.ExpandWidth(false)))
                        helpDialogFocusedMod = mod;
                }
            }

            GUILayout.FlexibleSpace();
            GUILayout.Label("Press Ctrl-H to close this dialog.");

            GUILayout.EndScrollView();
            GUILayout.EndArea();
        }

        /// <summary>
        /// Draws all the on-screen messages.
        /// </summary>
        void DisplayMessages()
        {
            // remove old messages
            KeyValuePair<float, string> msg;
            while (logMessages.Count > 0)
            {
                msg = logMessages.Peek();
                if (msg.Key < Time.unscaledTime)
                    logMessages.Dequeue();
                else
                    break;
            }

            // print all messages
            Rect rect = new Rect(18, 18, Screen.width, Screen.height);
            GUILayout.BeginArea(rect);
            GUILayout.BeginVertical();
            foreach (KeyValuePair<float, string> kvp in logMessages)
                GUILayout.Label(kvp.Value);
            GUILayout.EndVertical();
            GUILayout.EndArea();
        }

        /// <summary>
        /// Logs a message on-screen. The message disappears after a few seconds.
        /// </summary>
        /// <param name="message"></param>
        public void Log(string message, float displayTime = -1)
        {
            Debug.Log(message);
            if (displayTime <= 0)
                displayTime = messagesDisplayTime;
            logMessages.Enqueue(new KeyValuePair<float, string>(Time.unscaledTime + displayTime, message));
        }

        /// <summary>
        /// Logs a warning message on-screen. The message disappears after a few seconds.
        /// </summary>
        /// <param name="message"></param>
        public void LogWarning(string message, float displayTime = -1)
        {
            Debug.LogWarning(message);
            if (displayTime <= 0)
                displayTime = messagesDisplayTime;
            logMessages.Enqueue(new KeyValuePair<float, string>(Time.unscaledTime + displayTime, "<color=yellow>" + message + "</color>"));
        }

        /// <summary>
        /// Logs an error message on-screen. The message disappears after a few seconds.
        /// </summary>
        /// <param name="message"></param>
        public void LogError(string message, float displayTime = -1)
        {
            Debug.LogError(message);
            if (displayTime <= 0)
                displayTime = messagesDisplayTime;
            logMessages.Enqueue(new KeyValuePair<float, string>(Time.unscaledTime + displayTime, "<color=red>" + message + "</color>"));
        }

#pragma warning disable IDE0051
        void OnDestroy()
#pragma warning restore IDE0051
        {
            if (Instance == this)
                Instance = null;
        }
    }
}
