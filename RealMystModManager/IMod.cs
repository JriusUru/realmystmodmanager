namespace RealMystModManager
{
    /// <summary>
    /// Defines a class which implements MonoBehaviour, and acts like a mod.
    /// To maximize compatibility with other libraries, IMod is an interface and not an abstract extension of MonoBehaviour,
    /// however any class implementing IMod MUST extend MonoBehaviour.
    /// "Mod" MonoBehaviour are placed onto a single GameObject in the DontDestroyOnLoad scene when the mod is loaded and active.
    /// 
    /// You can use the Start method of your MonoBehaviour to setup your mod.
    /// Only one IMod is allowed per DLL.
    /// </summary>
    public interface IMod
    {
        /// <summary>
        /// The name of your mod, when displayed into the mod manager interface.
        /// </summary>
        string UserFriendlyName { get; }

        /// <summary>
        /// A description of what your mod does.
        /// Can also be used as a help sheet for the user.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Name of the author of this mod. This is only displayed in the mod list.
        /// </summary>
        string Author { get; }

        /// <summary>
        /// Version of the mod. This is only displayed in the mod list. It may be useful to increase anytime your mod changes,
        /// to help people know which version they have installed.
        /// </summary>
        float Version { get; }

        /// <summary>
        /// URL to the mod's main webpage.
        /// This is completely optional. This will let users open the webpage from the mod list.
        /// </summary>
        string Url { get; }

        /// <summary>
        /// Called in the mod info panel. This allows you to draw custom elements using IMGUI.
        /// Can be useful if you want to draw debug informations or options and such,
        /// when the <see cref="Description"/> is not enough.
        /// </summary>
        void DrawCustomHelpSheet();
    }
}
