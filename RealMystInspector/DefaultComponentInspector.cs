using System;
using System.Reflection;
using UnityEngine;

namespace RealMystInspector
{
    /// <summary>
    /// The default way to display a component's properties in the runtime inspector.
    /// This simply relies on reflection to find public fields/properties.
    /// </summary>
    public class DefaultComponentInspector : IComponentInspector
    {
        public bool Inspect(Component comp)
        {
            InspectFields(comp);
            InspectProperties(comp);

            // always return true, since reflection works for all components.
            return true;
        }

        /// <summary>
        /// Draws all displayable fields.
        /// </summary>
        /// <param name="comp"></param>
        void InspectFields(Component comp)
        {
            foreach (FieldInfo field in comp.GetType().GetFields())
            {
                if (field.FieldType.IsArray)
                {
                    // Exposing those in the inspector will be way too annoying for now.
                    continue;
                }

                if ((field.IsPublic || field.IsDefined(typeof(SerializeField), true)) && !field.IsDefined(typeof(ObsoleteAttribute), true))
                {
                    if (field.FieldType == typeof(int))
                    {
                        GUILayout.BeginHorizontal();
                        GUILayout.Label("(i) " + field.Name);
                        GUILayout.FlexibleSpace();
                        int oldVal = (int)field.GetValue(comp);
                        string newVal = GUILayout.TextField(oldVal.ToString());
                        if (newVal != oldVal.ToString())
                        {
                            if (int.TryParse(newVal, out int newValInt))
                                field.SetValue(comp, newValInt);
                        }
                        GUILayout.EndHorizontal();
                    }
                    else if (field.FieldType == typeof(float))
                    {
                        GUILayout.BeginHorizontal();
                        GUILayout.Label("(f) " + field.Name);
                        GUILayout.FlexibleSpace();
                        float oldVal = (float)field.GetValue(comp);
                        string newVal = GUILayout.TextField(oldVal.ToString());
                        if (newVal != oldVal.ToString())
                        {
                            if (float.TryParse(newVal, out float newValFloat))
                                field.SetValue(comp, newValFloat);
                        }
                        GUILayout.EndHorizontal();
                    }
                    else if (field.FieldType == typeof(bool))
                    {
                        GUILayout.BeginHorizontal();
                        bool oldValue = (bool)field.GetValue(comp);
                        GUILayout.Label("(b) " + field.Name);
                        GUILayout.FlexibleSpace();
                        if (GUILayout.Toggle(oldValue, "") != oldValue)
                            field.SetValue(comp, !oldValue);
                        GUILayout.EndHorizontal();
                    }
                    else if (field.FieldType == typeof(string))
                    {
                        GUILayout.BeginHorizontal();
                        GUILayout.Label("(s) " + field.Name);
                        GUILayout.FlexibleSpace();
                        string oldVal = (string)field.GetValue(comp);
                        string newVal = GUILayout.TextField(oldVal);
                        if (newVal != oldVal)
                            field.SetValue(comp, newVal);
                        GUILayout.EndHorizontal();
                    }
                    else
                    {
                        GUILayout.BeginHorizontal();
                        GUILayout.Label("(?) " + field.Name + ": ");
                        GUILayout.FlexibleSpace();
                        object value = field.GetValue(comp);
                        if (value != null)
                            GUILayout.Label(value.ToString());
                        else
                            GUILayout.Label("[null]");
                        GUILayout.EndHorizontal();
                    }
                }
            }
        }

        /// <summary>
        /// Draws all displayable properties.
        /// </summary>
        /// <param name="comp"></param>
        void InspectProperties(Component comp)
        {
            foreach (PropertyInfo prop in comp.GetType().GetProperties())
            {
                MethodInfo getMethod = prop.GetGetMethod();

                if (prop.PropertyType.IsArray || prop.GetIndexParameters().Length != 0)
                {
                    // Exposing those in the inspector will be way too annoying for now...
                    continue;
                }

                if ((getMethod != null && getMethod.IsPublic || prop.IsDefined(typeof(SerializeField), true)) && !prop.IsDefined(typeof(ObsoleteAttribute), true))
                {
                    if (getMethod.ReturnType == typeof(int))
                    {
                        GUILayout.BeginHorizontal();
                        GUILayout.Label("(i) " + prop.Name);
                        GUILayout.FlexibleSpace();
                        MethodInfo setMethod = prop.GetSetMethod();
                        bool canChange = setMethod != null && setMethod.IsPublic;
                        int curValue = (int)getMethod.Invoke(comp, new object[0]);
                        if (canChange)
                        {
                            string newVal = GUILayout.TextField(curValue.ToString());
                            if (canChange && curValue.ToString() != newVal)
                            {
                                if (int.TryParse(newVal, out int newValInt))
                                    setMethod.Invoke(comp, new object[] { newValInt });
                            }
                        }
                        else
                            GUILayout.Label(curValue.ToString());
                        GUILayout.EndHorizontal();
                    }
                    else if (getMethod.ReturnType == typeof(float))
                    {
                        GUILayout.BeginHorizontal();
                        GUILayout.Label("(f) " + prop.Name);
                        GUILayout.FlexibleSpace();
                        MethodInfo setMethod = prop.GetSetMethod();
                        bool canChange = setMethod != null && setMethod.IsPublic;
                        float curValue = (float)getMethod.Invoke(comp, new object[0]);
                        if (canChange)
                        {
                            string newVal = GUILayout.TextField(curValue.ToString());
                            if (canChange && curValue.ToString() != newVal)
                            {
                                if (float.TryParse(newVal, out float newValFloat))
                                    setMethod.Invoke(comp, new object[] { newValFloat });
                            }
                        }
                        else
                            GUILayout.Label(curValue.ToString());
                        GUILayout.EndHorizontal();
                    }
                    else if (getMethod.ReturnType == typeof(bool))
                    {
                        GUILayout.BeginHorizontal();
                        MethodInfo setMethod = prop.GetSetMethod();
                        bool canChange = setMethod != null && setMethod.IsPublic;
                        bool oldValue = (bool)getMethod.Invoke(comp, new object[0]);
                        GUILayout.Label("(b) " + prop.Name);
                        GUILayout.FlexibleSpace();
                        if (canChange)
                        {
                            if (GUILayout.Toggle(oldValue, "") != oldValue)
                                setMethod.Invoke(comp, new object[] { !oldValue });
                        }
                        else
                            GUILayout.Toggle(oldValue, "(read only)");
                        GUILayout.EndHorizontal();
                    }
                    else if (getMethod.ReturnType == typeof(string))
                    {
                        GUILayout.BeginHorizontal();
                        GUILayout.Label("(s) " + prop.Name);
                        GUILayout.FlexibleSpace();
                        MethodInfo setMethod = prop.GetSetMethod();
                        bool canChange = setMethod != null && setMethod.IsPublic;
                        string curValue = (string)getMethod.Invoke(comp, new object[0]);
                        if (canChange)
                        {
                            string newVal = GUILayout.TextField(curValue);
                            if (canChange && curValue != newVal)
                                setMethod.Invoke(comp, new object[] { newVal });
                        }
                        else
                            GUILayout.Label(curValue);
                        GUILayout.EndHorizontal();
                    }
                    else
                    {
                        GUILayout.BeginHorizontal();
                        GUILayout.Label("(?) " + prop.Name + ": ");
                        GUILayout.FlexibleSpace();
                        object value = getMethod.Invoke(comp, new object[0]);
                        if (value != null)
                            GUILayout.Label(value.ToString());
                        else
                            GUILayout.Label("[null]");
                        GUILayout.EndHorizontal();
                    }
                }
            }
        }
    }
}
