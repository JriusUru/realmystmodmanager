using RealMystModManager;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

namespace RealMystInspector
{
    /// <summary>
    /// Provides a very basic hierarchy/inspector to runtime Unity.
    /// </summary>
    public class Inspector : MonoBehaviour, IMod
    {
        public string UserFriendlyName => "Hierarchy and inspector";

        public string Description => @"Provides a very barebone hierarchy and inspector panel at runtime.
Press Ctrl+K to open.
The left panel is the hierarchy, the right panel is the inspector.";

        public string Author => "Sirius";

        public float Version => 1.0f;

        public string Url => "https://gitlab.com/JriusUru/realmystmodmanager";

        /// <summary>
        /// List of component inspectors. When a component is displayed on screen,
        /// the first compatible inspector from this list is used to display the component's fields.
        /// By default a "default" inspector is provided, which relies on reflection to expose fields and properties (it is quite verbose though).
        /// If you want to use a custom inspector for a specific component, insert an object implementing <see cref="IComponentInspector"/>
        /// at the beginning of the list.
        /// </summary>
        public List<IComponentInspector> componentInspectors = new List<IComponentInspector>() { new DefaultComponentInspector() };

        string searchPattern = "";

        readonly List<GameObject> rootGos = new List<GameObject>();
        readonly List<GameObject> allGos = new List<GameObject>();
        List<GameObject> filteredGos = null;
        GameObject inspectedGo;
        GUIStyle hierarchyBtnSkin;
        GUIStyle hierarchyGreyBtnSkin;
        GUIStyle toggleDisabledSkin;
        GUIStyle toggleLeftAlignSkin;
        GUIStyle componentZebraBlackSkin;
        GUIStyle componentZebraWhiteSkin;
        bool initialized = false;
        string newLayerString = "";
        Vector2 hierarchyScroll;
        Vector2 inspectorScroll;
        readonly Dictionary<GameObject, bool> expandedGos = new Dictionary<GameObject, bool>();
        bool displayed = false;
        bool invalidRegex = false;
        readonly Dictionary<Component, bool> inspectorExpandedComponents = new Dictionary<Component, bool>();

#pragma warning disable IDE0051
        void Start()
#pragma warning restore IDE0051
        {
            RefreshSceneHierarchy();
            RefreshHierarchyFilter();
        }

#pragma warning disable IDE0051
        void Update()
#pragma warning restore IDE0051
        {
            if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && Input.GetKeyUp(KeyCode.K))
                // toggle display
                displayed = !displayed;
        }

        /// <summary>
        /// Renders both the hierarchy and the inspector.
        /// </summary>
#pragma warning disable IDE0051
        void OnGUI()
#pragma warning restore IDE0051
        {
            if (!displayed)
                return;

            if (initialized == false)
            {
                // we gotta fetch a few skins before we can begin drawing...

                hierarchyBtnSkin = new GUIStyle(GUI.skin.label);
                hierarchyGreyBtnSkin = new GUIStyle(GUI.skin.label);
                toggleDisabledSkin = new GUIStyle(GUI.skin.toggle);
                toggleLeftAlignSkin = new GUIStyle(GUI.skin.toggle)
                {
                    alignment = TextAnchor.UpperLeft
                };

                componentZebraBlackSkin = new GUIStyle(GUI.skin.box);
                componentZebraBlackSkin.normal.background = MakeTex(8, 8, new Color(.2f, .2f, .2f, .7f));
                componentZebraWhiteSkin = new GUIStyle(GUI.skin.box);
                componentZebraWhiteSkin.normal.background = MakeTex(8, 8, new Color(.3f, .3f, .3f, .7f));

                hierarchyBtnSkin.normal.textColor = Color.white;
                hierarchyBtnSkin.alignment = TextAnchor.UpperLeft;
                hierarchyGreyBtnSkin.normal.textColor = Color.grey;
                hierarchyGreyBtnSkin.alignment = TextAnchor.UpperLeft;

                hierarchyBtnSkin.onHover.textColor =
                    hierarchyBtnSkin.onActive.textColor =
                    hierarchyBtnSkin.active.textColor =
                    hierarchyBtnSkin.hover.textColor =
                    hierarchyGreyBtnSkin.onHover.textColor =
                    hierarchyGreyBtnSkin.onActive.textColor =
                    hierarchyGreyBtnSkin.active.textColor =
                    hierarchyGreyBtnSkin.hover.textColor =
                        Color.yellow;

                toggleDisabledSkin.normal.textColor = Color.grey;
                toggleDisabledSkin.hover.textColor = Color.grey;
                toggleDisabledSkin.active.textColor = Color.grey;
                toggleDisabledSkin.focused.textColor = Color.grey;
                toggleDisabledSkin.onActive.textColor = Color.grey;
                toggleDisabledSkin.onFocused.textColor = Color.grey;
                toggleDisabledSkin.onHover.textColor = Color.grey;
                toggleDisabledSkin.onNormal.textColor = Color.grey;
                
                initialized = true;
            }

            DrawHierarchy();
            DrawInspector();
        }

        /// <summary>
        /// Draws the hierarchy on the left side of the screen.
        /// </summary>
        void DrawHierarchy()
        {
            // mandatory: check for "null" (using Unity's crappy overloaded operator) objects.
            // Those are deleted/unloaded objects. If any exists, we should refresh the hierarchy to avoid nullptr exceptions
            foreach (GameObject go in allGos)
            {
                if (go == null)
                {
                    RefreshSceneHierarchy();
                    break;
                }
            }

            // draw the main panel
            Rect rect = new Rect(0, 0, Screen.width / 4, Screen.height);
            GUILayout.BeginArea(rect, GUI.skin.box);

            // draw the search field and refresh hierarchy button
            string oldSearchPattern = searchPattern;
            searchPattern = GUILayout.TextField(searchPattern);
            if (GUILayout.Button("Refresh hierarchy"))
                RefreshSceneHierarchy();
            if (searchPattern != oldSearchPattern)
                RefreshHierarchyFilter();

            hierarchyScroll = GUILayout.BeginScrollView(hierarchyScroll);
            if (!string.IsNullOrEmpty(searchPattern))
            {
                if (invalidRegex)
                {
                    GUILayout.Label("Search filter is not a valid regular expression.");
                }
                else
                {
                    // We're in search mode. Show all filtered objects without taking hierarchy into account.
                    foreach (GameObject go in filteredGos)
                    {
                        if (GUILayout.Button(go.name, go.activeInHierarchy ? hierarchyBtnSkin : hierarchyGreyBtnSkin))
                            InspectGo(go);
                    }
                }
            }
            else
            {
                // We're in the standard hierarchy display. Draw all gameobjects.
                foreach (GameObject go in rootGos)
                    DrawHierarchyGo(go, 0);
            }

            GUILayout.EndScrollView();
            GUILayout.EndArea();
        }

        /// <summary>
        /// Draws the inspector on the right side of the screen.
        /// </summary>
        void DrawInspector()
        {
            if (inspectedGo == null)
                return;

            // draw the main panel
            Rect rect = new Rect(Screen.width * 3 / 4, 0, Screen.width / 4, Screen.height);
            GUILayout.BeginArea(rect, GUI.skin.box);
            inspectorScroll = GUILayout.BeginScrollView(inspectorScroll);

            GUILayout.Label("Inspecting object: " + inspectedGo.name);

            GUILayout.Space(18);

            // draw some properties of the gameObject itself...

            GUILayout.BeginHorizontal();
            GUILayout.Label("Layer: " + inspectedGo.layer + " <" + LayerMask.LayerToName(inspectedGo.layer) + ">");
            newLayerString = GUILayout.TextField(newLayerString);
            if (newLayerString != inspectedGo.layer.ToString())
            {
                if (int.TryParse(newLayerString, out int newLyr) && newLyr >= 0 && newLyr < 32)
                    inspectedGo.layer = newLyr;
            }
            GUILayout.EndHorizontal();

            bool active = GUILayout.Toggle(inspectedGo.activeSelf, "Active (self): ");
            if (active != inspectedGo.activeSelf)
                inspectedGo.SetActive(active);
            GUILayout.Toggle(inspectedGo.activeInHierarchy, "Active (hierarchy): ", toggleDisabledSkin);

            GUILayout.Space(18);

            // draw the list of components

            GUILayout.Label("Components:");

            bool white = false;
            foreach (Component comp in inspectedGo.GetComponents<Component>())
            {
                GUILayout.BeginVertical(white ? componentZebraWhiteSkin : componentZebraBlackSkin);
                white = !white;

                // check if the component is in the list of expanded components...
                // (note that we never clear this list - tracking all expanded components in all objects should be no big deal.)
                if (inspectorExpandedComponents.TryGetValue(comp, out bool expanded))
                {
                    expanded = GUILayout.Toggle(expanded, comp.GetType().Name);
                    inspectorExpandedComponents[comp] = expanded;
                }
                else
                {
                    if (GUILayout.Toggle(false, comp.GetType().Name))
                        inspectorExpandedComponents[comp] = true;
                }

                if (expanded)
                {
                    foreach (IComponentInspector inspector in componentInspectors)
                    {
                        if (inspector.Inspect(comp))
                        {
                            // this inspector reported that it was able to inspect the component, so we can safely break.
                            break;
                        }
                    }
                }

                GUILayout.EndVertical();
            }

            GUILayout.EndScrollView();
            GUILayout.EndArea();
        }

        /// <summary>
        /// Draws a gameobject's branch in the hierarchy panel.
        /// </summary>
        /// <param name="go"></param>
        /// <param name="indent"></param>
        void DrawHierarchyGo(GameObject go, int indent)
        {
            GUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
            GUILayout.Space(indent * 20); // offset the go's name

            // If we have children, draw a checkbox allowing expanding the hierarchy.
            // The hierarchy is collapsed by default - this is required since IMGUI is slow as crap when using lots of objects...
            if (go.transform.childCount != 0)
            {
                if (!expandedGos.ContainsKey(go))
                    expandedGos[go] = false;
                bool curExpanded = expandedGos[go];
                bool toggled = GUILayout.Toggle(curExpanded, "", toggleLeftAlignSkin, GUILayout.ExpandWidth(false));
                if (toggled != curExpanded)
                    expandedGos[go] = toggled;
            }
            else
                GUILayout.Space(20);

            if (GUILayout.Button(go.name, go.activeInHierarchy ? hierarchyBtnSkin : hierarchyGreyBtnSkin, GUILayout.ExpandWidth(true)))
            {
                // The object's name was pressed - inspect it.
                InspectGo(go);
            }
            
            GUILayout.EndHorizontal();

            if (expandedGos.TryGetValue(go, out bool expanded) && expanded)
            {
                // The object's hierarchy is expanded. Draw its children.
                foreach (Transform child in go.transform)
                    DrawHierarchyGo(child.gameObject, indent + 1);
            }
        }

        /// <summary>
        /// Re-runs filtering of the hierarchy based on the <see cref="searchPattern"/>.
        /// The filtering string is assumed to be a regular expression.
        /// </summary>
        void RefreshHierarchyFilter()
        {
            if (string.IsNullOrEmpty(searchPattern))
            {
                filteredGos = null;
                return;
            }

            if (filteredGos != null)
                filteredGos.Clear();
            else
                filteredGos = new List<GameObject>();

            Regex regex;
            try
            {
                regex = new Regex(searchPattern, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            }
            catch (ArgumentException)
            {
                invalidRegex = true;
                return;
            }
            invalidRegex = false;

            foreach (GameObject go in allGos)
            {
                if (regex.IsMatch(go.name))
                    filteredGos.Add(go);
            }
        }

        /// <summary>
        /// Parses the whole scene hierarchy once again to find all objects currently in the scene. May take a while.
        /// </summary>
        void RefreshSceneHierarchy()
        {
            rootGos.Clear();
            allGos.Clear();
            expandedGos.Clear();
            foreach (Transform tr in FindObjectsOfType<Transform>())
            {
                if (tr.parent == null)
                    rootGos.Add(tr.gameObject);
                allGos.Add(tr.gameObject);
            }
        }

        /// <summary>
        /// Sets the object currently displayed in the inspector.
        /// </summary>
        /// <param name="go"></param>
        void InspectGo(GameObject go)
        {
            inspectedGo = go;
            newLayerString = go.layer.ToString();
        }

        /// <summary>
        /// Simple way to generate a background texture of given color.
        /// (Because imgui doesn't support background colors, which is annoying.)
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        private Texture2D MakeTex(int width, int height, Color col)
        {
            Color[] pix = new Color[width * height];

            for (int i = 0; i < pix.Length; i++)
                pix[i] = col;

            Texture2D result = new Texture2D(width, height);
            result.SetPixels(pix);
            result.Apply();

            return result;
        }

        public void DrawCustomHelpSheet() { } // nothing to display, really...
    }
}
