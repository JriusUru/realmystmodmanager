using UnityEngine;

namespace RealMystInspector
{
    /// <summary>
    /// An object which can inspect Unity <see cref="Component"/>s in the runtime inspector.
    /// </summary>
    public interface IComponentInspector
    {
        /// <summary>
        /// Inspects (=draw fields and properties using IMGUI) the given component.
        /// </summary>
        /// <param name="comp">The component to inspect.</param>
        /// <returns>True if this inspector supports this object type, otherwise false.</returns>
        bool Inspect(Component comp);
    }
}
