Use dnSpy to load Assembly-CSharp.dll. In Intro.cs, method Start(), paste the following body. Compile, save, etc.

private void Start()
{
    // Find the mod manager code. This is usually located in the persistent data paths (AppData/LocalLow/Cyan Worlds/realMyst).
    string managerPath = Path.Combine(Application.persistentDataPath, "RealMystModManager.dll");
    if (File.Exists(managerPath))
    {
        MonoBehaviour.print("--[Loading mod manager at " + managerPath + "]--");
        // Load the assembly into the current domain and create the mod manager object.
        GameObject gameObject = new GameObject("Mod manager");
        Assembly assembly = Assembly.LoadFrom(managerPath);
        gameObject.AddComponent(assembly.GetType("RealMystModManager.ModManager"));
                // The mod manager will handle setting itself as not destroyed on load, and load other mods by itself.
        return;
    }
    Debug.LogError("--[Couldn't find mod manager at " + managerPath + "]--");
}