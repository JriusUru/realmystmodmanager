# RealMyst Mod Manager

This is a simple mod loader/manager for realMyst: Masterpiece Edition. It also includes a few useful mods:
* Flymode
* Runtime hierarchy and inspector
* ZQSD support

More details below.

## Version

Current version is designed to work with the following RMME version:

```
BuildID (Steam Properties → Local Files): 3136950
File ID (realMyst.exe properties): 4.7.2.65147
Product ID (realMyst.exe properties): 4.7.2.11009659
```
Mods and the mod manager themselves are usually version-agnostic. They however require a modification to a RMME file (`Assembly-CSharp.dll`), which is version dependent. Don't use this file on a different version of realMyst - this is likely to cause issues.

Currently, only the Windows version of the game is supported (it might work on other OSes, but it's untested).

## Installing

Download the latest version from the [Releases](https://gitlab.com/JriusUru/realmystmodmanager/-/releases) section.

* You'll need to copy the file `Assembly-CSharp.dll` into realMyst's `Managed` folder. Usually, this is located inside `C:\Program Files (x86)\Steam\SteamApps\common\realMyst Masterpiece Edition\realMyst_Data\Managed`. Overwrite when asked (you can rename the original file to keep it around if you prefer).
* Then, go to `C:\Users\<your username>\AppData\LocalLow\Cyan Worlds\realMyst`. Paste `RealMystModManager.dll` as well as the `mods` folder.
* Inside the `mods` folder, you can delete the DLL of any mod you're not interested in. If you are using a WASD keyboard, you usually want to remove `RealMystZQSD.dll`.

Start the game, and you should see new text displayed during the intro logo, indicating the mod manager is working.

## Uninstalling

Revert `Assembly-CSharp.dll` if you have a backup, or delete `RealMystModManager.dll`, or validate the game files with Steam.

## Usage

When ingame, the mod manager provides a helpful list of loaded mods. You can toggle this list by pressing Ctrl-H while ingame. This list gives a few details about the mods themselves, so it's worth checking it out if you're not sure how a particular mod works.

Mods are always loaded on startup from DLLs inside the `AppData\LocalLow\Cyan Worlds\realMyst\mods` folder.

## Mods available

Here is a small description of how to use/activate each mod currently available. Remember that the help sheet available with Ctrl-H will have most of these informations ingame.

### Flymode

Does exactly what it says. RMME actually has a flymode feature, which is normally inaccessible in the final game. This mod allows you to enable with, with a few enhancements.
* Toggle flymode ingame using the "G" key.
* "C" toggle collision while flying. This also includes player detection, which is used to show/hide parts of the Age during exploration.
* Ctrl + scroll wheel changes player movement speed. This works both during flymode and when walking.
* "U" will attempt to unblock the player when it is locked by a puzzle. Due to how puzzles are coded, this does not always work.

**Warning**: exiting the game while using flymode may toast the automatic save. Don't do it.

### ZQSD

Simply allows you to strafe left and move forward using the Q/Z keys on your keyboard, which AZERTY keyboard users will find useful. If you're not using a similar keyboard layout, simply delete the mod.

Note that due to how Cyan's flymode is coded, pressing Q with this mod loaded will both strafe left AND move you down while flying.

### Runtime inspector and hierarchy

This is similar to Unity's own hierarchy and inspector panes, but is rendered at realtime using IMGUI and thus is more limited in features. It's still incredibly useful to mess around with the game.
Remember though: most of what you do with it will NOT be saved when you restart the game.

Use Ctrl-K to open/close. I won't detail how it works, if you're used to Unity it should be fairly self-explanatory.

Known limitations:
* It relies on reflection to expose fields in the inspector. As such, it's not as clean as Unity's own inspector and will display a LOT of stuff.
* Performances are pretty bad due to drawing with IMGUI. As long as you don't expand all panels, you should be fine.
* Editing float/int/string properties in the inspector will apply the new string value on each keystroke, which may be undesirable. This is an unfortunate side-effect of IMGUI. Copy-pasting text to and from a notepad outside the game is a viable solution.

## Want to make your own mod ?

The quickest way is to refer to how existing mods are coded to figure out how to setup your own mod. The code is extensively documented, which should help you get started.

Things to keep in mind:
* Mods are mostly code. Using new assets will either require Unity `AssetBundles` (unfortunately a paid feature of the Unity version RMME uses), or a custom serialization system.
* Mods can have other DLL dependencies, which should be placed in the `mods\dependencies` folder.
* You can view Unity's output log from the `output_log.txt` in RMME's `realMyst_Data` folder.
* Cyan's own scripts are compiled in `Assembly-CSharp.dll` and can't be directly modified. You can however use [dnSpy](https://github.com/0xd4d/dnSpy) to decompile it. Then, you can use C# reflection in your own mod to mess with private properties, or even use [Harmony](https://github.com/pardeike/Harmony) (not currently included in the project) to rewrite methods. Direct modification to this DLL is undesirable since it can't be easily redistributed.

## Upgrading Assembly-CSharp.dll

This file changes with each version of the game, thus removing the "hack" that loads the mod manager. Patching it again is pretty simple using [dnSpy](https://github.com/0xd4d/dnSpy). Refer to the `modmanagerloader.txt` file for more info.