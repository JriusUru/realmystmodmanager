using RealMystModManager;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RealMystZQSD
{
    /// <summary>
    /// Provides ZQSD as movement for French keyboards and similar variants.
    /// Known issues: flymode is hardcoded to use Input.GetKey instead of the InputProcessor (booo !),
    /// so going left in flymode will also lower the player.
    /// Workaround: hijack Input.GetKey via Harmony. This would lock us out of mod deactivation. (Not a big deal though,
    /// since we don't have AppDomains anyway...)
    /// </summary>
    public class Zqsd : MonoBehaviour, IMod
    {
        public string UserFriendlyName => "ZQSD movements";

        public string Description => @"Replaces WASD with ZQSD for movement. Useful for French and similar keyboards.
Note that this doesn't work too well with flymode - the Q key will both move the player left and lower it.";

        public string Author => "Sirius";

        public float Version => 1.0f;

        public string Url => "https://gitlab.com/JriusUru/realmystmodmanager";

        public bool Active
        {
            get => enabled;
            set
            {
                bool oldEnabled = enabled;
                enabled = value;
                if (enabled != oldEnabled)
                {
                    if (enabled)
                        StartCoroutine(WaitInit());
                    else
                        Deactivate();
                }
            }
        }

#pragma warning disable IDE0051
        void Start()
        {
            StartCoroutine(WaitInit());
        }

        void OnLevelWasLoaded(int _)
        {
            if (Active)
                StartCoroutine(WaitInit());
        }
#pragma warning restore IDE0051

        /// <summary>
        /// Wait for the GameLogic script to be initialized.
        /// This is required because mods are loaded during the Cyan logo before most of the actual game.
        /// (This could be written in a better way, but it works fine.)
        /// </summary>
        /// <returns></returns>
        IEnumerator WaitInit()
        {
            while (GameLogic.Instance == null || GameLogic.Instance.MystControllerRef == null || GameLogic.Instance._ActionKeys == null)
                yield return null;
            Activate();
        }

        void Activate()
        {
            AddKey(KeyCode.W, KeyCode.Z);
            AddKey(KeyCode.A, KeyCode.Q);
        }

        void Deactivate()
        {
            RemoveKey(KeyCode.W, KeyCode.Z);
            RemoveKey(KeyCode.A, KeyCode.Q);
        }

        private void AddKey(KeyCode origKey, KeyCode newKey)
        {
            foreach (var kvp in GameLogic.Instance._ActionKeys)
            {
                if (kvp.Value.Contains(origKey) && !kvp.Value.Contains(newKey))
                {
                    KeyCode[] newArr = new KeyCode[kvp.Value.Length + 1];
                    Array.Copy(kvp.Value, 0, newArr, 0, kvp.Value.Length);
                    newArr[kvp.Value.Length] = newKey;
                    GameLogic.Instance._ActionKeys[kvp.Key] = newArr;
                    break;
                }
            }
        }

        private void RemoveKey(KeyCode origKey, KeyCode newKey)
        {
            foreach (var kvp in GameLogic.Instance._ActionKeys)
            {
                if (kvp.Value.Contains(origKey))
                {
                    KeyCode[] newArr = new KeyCode[kvp.Value.Length - 1];
                    int i = 0;
                    foreach (var key in kvp.Value)
                    {
                        if (key != newKey)
                            newArr[i++] = key;
                    }
                    GameLogic.Instance._ActionKeys[kvp.Key] = newArr;
                    break;
                }
            }
        }

        public void DrawCustomHelpSheet() { } // nothing to display
    }
}
