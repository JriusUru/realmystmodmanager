using RealMystModManager;
using System.Reflection;
using UnityEngine;

namespace RealMystFlymode
{
    /// <summary>
    /// Activate's the <see cref="MystController"/>'s flymode.
    /// (This relies on Cyan's own flymode, which is a bit lacking but functional.)
    /// Additionally allows:
    /// - toggling player collisions (only when flymode is active)
    /// - unblock the player when it's softlocked in place
    /// - change player movement speed
    /// </summary>
    public class Flymode : MonoBehaviour, IMod
    {
        public string UserFriendlyName => "Flymode";
        
        public string Description => @"Allows flying in the game.
Press G to toggle flymode.
When flying, press C to toggle collision.
Use Ctrl + mouse wheel to adjust speed (also works outside flymode).
Use U to attempt to unblock the player (may not work for some puzzles).";

        public string Author => "Sirius";

        public float Version => 1.0f;

        public string Url => "https://gitlab.com/JriusUru/realmystmodmanager";

        FieldInfo cameraModeField;
        FieldInfo movementSpeedField;
        int ignoreRaycastLayer = -1;
        int playerDefaultLayer = -1;
        float defaultPlayerSpeed = -1;
        float currentSpeedLevel = 0;

#pragma warning disable IDE0051
        void Start()
#pragma warning restore IDE0051
        {
            // fetch private fields we'll be modifying
            cameraModeField = typeof(MystController).GetField("_CameraMode", BindingFlags.NonPublic | BindingFlags.Instance);
            movementSpeedField = typeof(MystController).GetField("_MaxMoveSpeed", BindingFlags.NonPublic | BindingFlags.Instance);
            // get the ignore raycast layer id
            ignoreRaycastLayer = LayerMask.NameToLayer("Ignore Raycast");
        }

#pragma warning disable IDE0051
        void Update()
#pragma warning restore IDE0051
        {
            if (Input.GetKeyDown(KeyCode.G))
            {
                // toggle flymode
                FlymodeActive = !FlymodeActive;
                if (!FlymodeActive)
                {
                    // restore collision and speed
                    CollisionActive = true;
                    PlayerMovementSpeed = defaultPlayerSpeed;
                    currentSpeedLevel = 0;
                }
                ModManager.Instance.Log("Flymode: " + FlymodeActive);
            }
            if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
            {
                if (Input.mouseScrollDelta.y != 0)
                {
                    // change movement speed
                    currentSpeedLevel += Input.mouseScrollDelta.y / 50f;
                    if (FlymodeActive)
                        currentSpeedLevel = Mathf.Clamp(currentSpeedLevel, -1.5f, .7f); // [3%, 500%] of the initial speed
                    else
                        currentSpeedLevel = Mathf.Clamp(currentSpeedLevel, -1.5f, .3f); // [3%, 200%] of the initial speed
                    PlayerMovementSpeed = defaultPlayerSpeed * Mathf.Pow(10, currentSpeedLevel);
                    ModManager.Instance.Log("Movement speed: " + PlayerMovementSpeed);
                }
            }
            if (Input.GetKeyDown(KeyCode.C) && FlymodeActive)
            {
                // toggles collision
                CollisionActive = !CollisionActive;
                ModManager.Instance.Log("Collision: " + CollisionActive);
            }
            if (Input.GetKeyDown(KeyCode.U))
            {
                // unblocks the player
                GameLogic.Instance.DisengagePuzzle();
                PlayerMovementBlock = false;
                PlayerDisabled = false;
                ModManager.Instance.Log("Tried to unblock the player.");
            }
        }

        public void DrawCustomHelpSheet()
        {
            GUILayout.Label("Flymode: " + FlymodeActive);
            GUILayout.Label("Collision: " + CollisionActive);
            GUILayout.Label("Movement speed: " + PlayerMovementSpeed);
        }

        /// <summary>
        /// Get/set whether the flymode is currently active.
        /// This relies on Cyan's own flymode code.
        /// </summary>
        public bool FlymodeActive
        {
            get
            {
                MystController mystController = GameLogic.Instance?.MystControllerRef;
                if (mystController == null)
                    return false;

                return (bool)cameraModeField.GetValue(mystController);
            }
            set
            {
                MystController mystController = GameLogic.Instance?.MystControllerRef;
                if (mystController == null)
                    return;

                cameraModeField.SetValue(mystController, !FlymodeActive);
            }
        }

        /// <summary>
        /// Get/set player collisions (by putting the player's GO onto the Ignore Raycast layer).
        /// Not recommended unless flymode is also active.
        /// Any use of this property will store the initial player's layer into <see cref="playerDefaultLayer"/>.
        /// </summary>
        public bool CollisionActive
        {
            get
            {
                MystController mystController = GameLogic.Instance?.MystControllerRef;
                if (mystController == null)
                    return true;

                int lyr = mystController.gameObject.layer;
                if (playerDefaultLayer < 0)
                    playerDefaultLayer = lyr;

                return lyr == playerDefaultLayer;
            }
            set
            {
                MystController mystController = GameLogic.Instance?.MystControllerRef;
                if (mystController == null)
                    return;

                int lyr = mystController.gameObject.layer;
                if (playerDefaultLayer < 0)
                    playerDefaultLayer = lyr;

                mystController.gameObject.layer = value ? playerDefaultLayer : ignoreRaycastLayer;
            }
        }

        /// <summary>
        /// Get/set whether the player is completely disabled.
        /// Setting it to false is obviously not recommended.
        /// </summary>
        public bool PlayerDisabled
        {
            get => GameLogic.Instance?.MystControllerRef?.Disabled ?? true;
            set
            {
                MystController mystController = GameLogic.Instance?.MystControllerRef;
                if (mystController == null)
                    return;

                mystController.Disabled = value;
                mystController.DisableRotate = value;
            }
        }

        /// <summary>
        /// Get/set whether the player's movement is locked.
        /// Setting it to false is not recommended.
        /// </summary>
        public bool PlayerMovementBlock
        {
            get => GameLogic.Instance?.MystControllerRef?.MovementBlock ?? true;
            set
            {
                MystController mystController = GameLogic.Instance?.MystControllerRef;
                if (mystController == null)
                    return;

                mystController.MovementBlock = value;
            }
        }

        /// <summary>
        /// Get/set the player's speed.
        /// Any use of this property will store the initial value into <see cref="defaultPlayerSpeed"/> (should always be 5 anyway).
        /// </summary>
        public float PlayerMovementSpeed
        {
            get
            {
                MystController mystController = GameLogic.Instance?.MystControllerRef;
                if (mystController == null)
                    return -1;

                float speed = (float)movementSpeedField.GetValue(mystController);
                if (defaultPlayerSpeed < 0)
                    defaultPlayerSpeed = speed; // cache the value for later use

                return speed;
            }
            set
            {
                MystController mystController = GameLogic.Instance?.MystControllerRef;
                if (mystController == null || value < 0)
                    return;

                if (defaultPlayerSpeed < 0)
                {
                    float speed = (float)movementSpeedField.GetValue(mystController);
                    defaultPlayerSpeed = speed; // cache the value for later use
                }

                movementSpeedField.SetValue(mystController, value);
            }
        }
    }
}
